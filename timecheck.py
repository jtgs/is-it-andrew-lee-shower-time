from datetime import datetime

def timecheck():
    now = datetime.now()
    today815 = now.replace(hour=8,minute=15,second=0,microsecond=0)
    today830 = now.replace(hour=8,minute=30,second=0,microsecond=0)

    if now >= today815 and now <= today830:
        return "YES"
    else:
        return "NO"

