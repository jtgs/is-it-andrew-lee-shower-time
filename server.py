from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from timecheck import *

# cfg
DEBUG = True

# create app
app = Flask(__name__)
app.config.from_object(__name__)

@app.route("/")
def home():
    result = timecheck()
    return render_template('home.html', result=result)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
